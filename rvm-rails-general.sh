#!/bin/bash

ABBREV='rvm-rails-general'
DISTRO='debian'
SUITE='buster'
VAGRANT_BOX_NAME="$DISTRO-$SUITE-$ABBREV"
VAGRANT_BOX_URL="http://downloads.sourceforge.net/project/vagrant-$DISTRO-$SUITE/$DISTRO-$SUITE-$ABBREV.box"
VIRTUALBOX_NAME="vagrant-$DISTRO-$SUITE-$ABBREV"

bash setup.sh $ABBREV $VAGRANT_BOX_NAME $VAGRANT_BOX_URL $VIRTUALBOX_NAME
