#!/bin/bash
# Parameter 1: abbreviation
# Parameter 2: name of Vagrant box
# Parameter 3: name of VirtualBox virtual machine
# Parameters 4 and 5: 1st guest port number and corresponding host port number
# Parameters 6 and 7: 2nd guest port number and corresponding host port number
# Parameters 8 and 9: 3rd . . . .
# Parameters 10 and 11: . . . .

############################################
# BEGIN: setting environment variable inputs
############################################

ABBREV=$1
shift # $2 becomes the new $1, $3 becomes the new $2, etc.
VAGRANT_BOX_NAME=$1
shift # $2 becomes the new $1, $3 becomes the new $2, etc.
VAGRANT_BOX_URL=$1
shift # $2 becomes the new $1, $3 becomes the new $2, etc.
VIRTUALBOX_NAME=$1
shift # $2 becomes the new $1, $3 becomes the new $2, etc.
LOCAL_BOX_FILE="$VAGRANT_BOX_NAME.box"

# Remaining parameters are port numbers

ARRAY_PORTS=() # NOTE: Always has an even number of elements
i=0
while [ $# -gt 1 ]; do # If the number of port numbers is odd, the last one is ignored.
  # ARRAY_PORTS+=($1 $2)
  ARRAY_PORTS[i]=$1
  ARRAY_PORTS[i+1]=$2
  shift # $2 becomes the new $1, $3 becomes the new $2, etc.
  shift # $2 becomes the new $1, $3 becomes the new $2, etc.
  i=$((i+2))
done

# echo "All elements of ARRAY_PORTS: ${ARRAY_PORTS[@]}"
# echo "Number of elements in ARRAY_PORTS: ${#ARRAY_PORTS[@]}"

###############################################
# FINISHED: setting environment variable inputs
###############################################

# Creating the necessary directories
mkdir -p $ABBREV
mkdir -p $ABBREV/shared

##########################
# BEGIN: copying use files
##########################

# Copy files for using Vagrant box
cp templates_use/* $ABBREV

#############################
# FINISHED: copying use files
#############################

#############################
# BEGIN: copying shared files
#############################

cp templates_shared/info.sh $ABBREV/shared # For all Vagrant boxes
cp templates_shared/README-host.txt $ABBREV/shared # For all Vagrant boxes

copy_all () {
  cp templates_shared/README-postgres.txt $ABBREV/shared
  cp templates_shared/pg-reset.sh $ABBREV/shared
  cp templates_shared/pg-setup.sh $ABBREV/shared
  cp templates_shared/test-react.sh $ABBREV/shared
  cp templates_shared/create-react-static.sh $ABBREV/shared
  cp templates_shared/test-docusaurus.sh $ABBREV/shared
  cp templates_shared/create-docusaurus.sh $ABBREV/shared
}

copy_rails_only () {
  cp templates_shared/test-rails-sq.sh $ABBREV/shared
  cp templates_shared/test-rails-pg.sh $ABBREV/shared
}

# For all images
copy_all

# For Rails images only
if [[ "$ABBREV" =~ 'rails' ]]
then
  copy_rails_only
fi

################################
# FINISHED: copying shared files
################################

#####################################################
# BEGIN: filling in the box and virtual machine names
#####################################################

# Fill in DOCKER_IMAGE and CONTAINER parameters
fill_in_params () {
  FILE_TO_UPDATE=$1
  # NOTE: Using \ instead of / as delimiter in sed command
  sed -i.bak "s|<VAGRANT_BOX_URL>|$VAGRANT_BOX_URL|g" $FILE_TO_UPDATE
  sed -i.bak "s|<VAGRANT_BOX_NAME>|$VAGRANT_BOX_NAME|g" $FILE_TO_UPDATE
  sed -i.bak "s|<VIRTUALBOX_NAME>|$VIRTUALBOX_NAME|g" $FILE_TO_UPDATE
  sed -i.bak "s|<ABBREV>|$ABBREV|g" $FILE_TO_UPDATE
  sed -i.bak "s|<LOCAL_BOX_FILE>|$LOCAL_BOX_FILE|g" $FILE_TO_UPDATE
  rm $FILE_TO_UPDATE.bak
}

fill_in_params $ABBREV/Vagrantfile

for FILE in `ls $ABBREV/*.sh`
do
  fill_in_params $FILE
done

for FILE in `ls $ABBREV/shared/*.sh`
do
  fill_in_params $FILE
done

# Provide Vagrant Box and VirtualBox virtual machine names when running the info.sh script
echo '-----------------------------------' > $ABBREV/shared/vagrant.txt
echo "Vagrant Box Name: $VAGRANT_BOX_NAME" >> $ABBREV/shared/vagrant.txt
echo '-----------------------------------------' >> $ABBREV/shared/vagrant.txt
echo "VirtualBox Machine Name: $VIRTUALBOX_NAME" >> $ABBREV/shared/vagrant.txt
echo '-----------------------------------------' >> $ABBREV/shared/vagrant.txt

########################################################
# FINISHED: filling in the box and virtual machine names
########################################################

#########################
# BEGIN: setting up ports
#########################

# Provide port numbers in shared/ports.txt file
echo '---------------------------------' > $ABBREV/shared/ports.txt
echo 'PORT FORWARDING (Host -> Vagrant)' >> $ABBREV/shared/ports.txt

i=0
LEN_ARRAY_PORTS=${#ARRAY_PORTS[@]}
INDEX_LAST=$((LEN_ARRAY_PORTS-1))
PORT_STRING="# PORT FORWARDING\n"
while [ $((i+1)) -le $((INDEX_LAST)) ]; do # If the number of port numbers is odd, the last one is ignored.
  P0=${ARRAY_PORTS[i]}
  P1=${ARRAY_PORTS[i+1]}
  PORT_STRING+="  config.vm.network :forwarded_port, guest: $P0, host: $P1\n"
  echo "$P0 -> $P1" >> $ABBREV/shared/ports.txt
  i=$((i+2))
done

sed -i.bak "s/#PORT FORWARDING/$PORT_STRING/g" $ABBREV/Vagrantfile
rm $ABBREV/Vagrantfile.bak

############################
# FINISHED: setting up ports
############################

echo '***************************************'
echo 'Enter the following command to proceed:'
echo "cd $ABBREV"
