#!/bin/bash

# Check for regular user login
if [ ! $( id -u ) -ne 0 ]; then
  echo 'You must be a regular user to run this script.'
  exit 2
fi

echo '---------------'
echo 'vagrant halt -f'
vagrant halt -f

sh login.sh
