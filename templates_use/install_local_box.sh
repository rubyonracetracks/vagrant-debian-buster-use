#!/bin/bash

# Check for regular user login
if [ ! $( id -u ) -ne 0 ]; then
  echo 'You must be a regular user to run this script.'
  exit 2
fi 

VAGRANT_BOX_NAME='<VAGRANT_BOX_NAME>'
LOCAL_BOX_FILE='<LOCAL_BOX_FILE>'

echo '---------------'
echo 'vagrant halt -f'
vagrant halt -f

echo '------------------'
echo 'vagrant destroy -f'
vagrant destroy -f

echo '--------------------------------------------'
echo "vagrant box remove $VAGRANT_BOX_NAME --force"
vagrant box remove $VAGRANT_BOX_NAME --force

echo '-----------------------------------------'
echo "vagrant box add $VAGRANT_BOX_NAME $LOCAL_BOX_FILE"
vagrant box add $VAGRANT_BOX_NAME $LOCAL_BOX_FILE;

sh login.sh
