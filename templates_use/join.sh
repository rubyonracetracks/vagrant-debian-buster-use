#!/bin/bash

# Check for regular user login
if [ ! $( id -u ) -ne 0 ]; then
  echo 'You must be a regular user to run this script.'
  exit 2
fi

echo '******'
echo 'NOTICE'
echo 'If this join.sh script does not work, then there is probably no'
echo 'running Vagrant box to join.  You likely need to run the'
echo 'download_new_box.sh, reset.sh, or login.sh script first.'
echo

echo '-----------'
echo 'vagrant ssh'
vagrant ssh
